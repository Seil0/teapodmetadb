# TeapodMetaDB

Metadata for Media from VoD Providers supported by Teapod

## Help us to expand the database
For TeapodMetaDB to support as many shows as possible, we need your help.
If you want to contribute to TeapodDB you need to be familiar with [tmdb](https://www.themoviedb.org/) and the developer tools of your browser.

For more details on how to acquire the needed data from the diffrenet services see: https://gitlab.com/Seil0/teapodmetadb/-/wikis/home

### Supported Providers

* Crunchyroll

### Licenses / Using MetaDB in other Projects
Since most of the data stored in MetaDB is not affected by copyright, singe metadata can be used as public domain.
If you however plan to use the database as a whole, it is licensed under CC BY-SA 4.0.

In any case of using MetaDB, it would be desirable, to give appropriate credit to TeapodMetaDB.
The more users know of MetaDB, the better and larger the database will be.
